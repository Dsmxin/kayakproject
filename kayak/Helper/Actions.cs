﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace kayak.Helper
{
    public class Actions
    {
        public static void ClickBy(By selector, IWebDriver driver)
        {
            FindBy.Selector(selector, driver);
            Thread.Sleep(Constants.ShortTimeout);
            driver.FindElement(selector).Click();
            Thread.Sleep(Constants.TimeoutAfterClick);
        }
        public static void SetTextBy(By selector, string text, IWebDriver driver)
        {
            FindBy.Selector(selector, driver);
            Thread.Sleep(Constants.ShortTimeout);
            driver.FindElement(selector).Clear();
            driver.FindElement(selector).SendKeys(text);
        } 
        public static void ExecuteScriptClick(By selector, IWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Constants.Timeout2));
            wait.Until(drv => drv.FindElement(selector));
            Thread.Sleep(Constants.ShortTimeout);
            IWebElement webElement = driver.FindElement(selector);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click()", webElement);
            Thread.Sleep(Constants.ShortTimeout);
        }
        public static void ClickPerform(By selector, IWebDriver driver)
        {
            IWebElement elementClick = driver.FindElement(selector);
            new OpenQA.Selenium.Interactions.Actions(driver).MoveByOffset(5, 5).Build().Perform();
            new OpenQA.Selenium.Interactions.Actions(driver).Click(elementClick).Build().Perform();
            Thread.Sleep(Constants.TimeoutAssert);
        }
        public static void SwitchToFirstWindowHandle(IWebDriver driver)
        {
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles[0]);
            Thread.Sleep(Constants.TimeoutAssert);
        }
        public static void SwitchToCurrentWindowHandleWithoutClose(IWebDriver driver)
        {
            string currentWindowHandle = driver.CurrentWindowHandle;
            string targetWindowHandle = "";
            foreach (string handle in driver.WindowHandles)
            {
                if (handle != currentWindowHandle)
                    targetWindowHandle = handle;
            }
            //driver.Close();
            driver.SwitchTo().Window(targetWindowHandle);
            Thread.Sleep(3000);
        }
        public static void ScrollTo(int xPosition, int yPosition, IWebDriver driver)
        {
            var js = String.Format("window.scrollTo({0}, {1})", xPosition, yPosition);
            ((IJavaScriptExecutor)driver).ExecuteScript(js);
        }
        public static IWebElement ScrollToView(By selector, IWebDriver driver)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            //wait.Until(ExpectedConditions.ElementExists(selector));
            wait.Until(drv => drv.FindElements(selector).Count > 0);

            var element = driver.FindElement(selector);
            ScrollToView(element, driver);
            return element;
        }
        public static void ClickWithScroll(By selector, IWebDriver driver)
        {
            ScrollToView(selector, driver);

            Actions.ClickBy(selector, driver);
        }
        public static void ScrollToView(IWebElement element, IWebDriver driver)
        {

            if (element.Location.Y > 200)
            {
                ScrollTo(0, element.Location.Y - 150, driver); // Make sure element is in the view but below the top navigation pane
            }
        }
    }
}
