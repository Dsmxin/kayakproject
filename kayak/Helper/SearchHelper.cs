﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kayak.Helper
{
    class SearchHelper
    {
        public static void SetFrom(string from, IWebDriver driver)
        {
            Actions.ClickBy(By.CssSelector(".lNCO-inner"), driver);//click on from field
            Actions.SetTextBy(By.CssSelector(".k_my-input"), from, driver);
            FindBy.Selector(By.CssSelector(".RQkB-mod-focused .JyN0-subName"), driver);
            driver.FindElement(By.CssSelector(".k_my-input")).SendKeys(Keys.Down);
            driver.FindElement(By.CssSelector(".k_my-input")).SendKeys(Keys.Down);
            driver.FindElement(By.CssSelector(".k_my-input")).SendKeys(Keys.Enter);
        }
        public static void SetSameDate(IWebDriver driver)
        {
            Actions.ClickBy(By.CssSelector(".cQtq-input:nth-child(1) .svg-image"), driver);
            Actions.ClickBy(By.CssSelector(".mkUa-isStartDate"), driver);
            Actions.ClickBy(By.CssSelector(".mkUa-isStartDate"), driver);
        }
        public static void SetStartTime(string time, IWebDriver driver)
        {
            if(!FindBy.XPath("//*[@class='ui-dialog-Popover Fp7U-popover Fp7U-mod-visible mod-layer-default']/div/div[2]/div/ul", driver).Displayed)
               Actions.ClickBy(By.CssSelector(".cQtq-input:nth-child(1) .c1jaM-display"), driver);
            var list = FindBy.XPath("//*[@class='ui-dialog-Popover Fp7U-popover Fp7U-mod-visible mod-layer-default']/div/div[2]/div/ul", driver); 
            list.FindElement(By.XPath("//li[text()='" + time + "']")).Click();
        }
        public static void SetEndTime(string time, IWebDriver driver)
        {
            driver.FindElement(By.CssSelector(".cQtq-input:nth-child(3) .c1jaM-display")).Click();
            FindBy.XPath("//*[@class='ui-dialog-Popover Fp7U-popover Fp7U-mod-visible mod-layer-default']/div/div[2]/div/ul", driver);
            Actions.ExecuteScriptClick(By.XPath("//*[@class='ui-dialog-Popover Fp7U-popover Fp7U-mod-visible mod-layer-default']/div/div[2]/div/ul/li[text()='" + time + "']"), driver);
        }
        public static void SelectOtherPlaceReturn(IWebDriver driver)
        {
            Actions.ClickBy(By.CssSelector(".wIIH-handle > span:nth-child(1)"), driver);
            Actions.ClickBy(By.CssSelector(".RQkB > li:nth-child(2)"), driver);//other place of return
        }
    }
}
