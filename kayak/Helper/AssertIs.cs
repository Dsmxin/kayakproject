using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace kayak.Helper
{
    public class AssertIs
    {
        public static void TextContainsInSelector(By selector, string text, IWebDriver driver, int sec = Constants.Timeout1)
        {
            FindBy.Selector(selector, driver);
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(sec));
                wait.Until(drv => drv.FindElement(selector).Text.ToLower().Contains(text.ToLower()));
            }
            catch (Exception)
            { throw new Exception("Text " + " " + text + " not found in " + selector.ToString()); }
        }
        
        public static bool ElementDisplayed(By selector, IWebDriver driver, int sec = Constants.Timeout1)
        {
            FindBy.Selector(selector, driver);
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(sec));
                wait.Until(drv => drv.FindElement(selector).Displayed);
            }
            catch (Exception)
            { }

            if (driver.FindElement(selector).Displayed)
                return true;
            else
                return false;
        }

    }


}
